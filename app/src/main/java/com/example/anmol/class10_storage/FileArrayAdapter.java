package com.example.anmol.class10_storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;
import java.util.Date;

/**
 * Created by anmol on 4/10/15.
 */
public class FileArrayAdapter extends ArrayAdapter<File>{
    Context context;
    public FileArrayAdapter(Context context, File[] objects) {
        super(context, 0, objects);
        this.context = context;
    }

    public class ViewHolder{
        ImageView iv;
        TextView tv;
        Button b;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.file_item_layout,null);
            ViewHolder holder = new ViewHolder();
            holder.iv = (ImageView) convertView.findViewById(R.id.fileDirIcon);
            holder.tv = (TextView) convertView.findViewById(R.id.fileName);
            holder.b = (Button) convertView.findViewById(R.id.fileButton);
            convertView.setTag(holder);
        }
        ViewHolder holder = (ViewHolder) convertView.getTag();
        File currentFile = getItem(position);
        if(currentFile.isDirectory()){
            holder.iv.setImageResource(android.R.drawable.ic_media_play);
        }else {
            holder.iv.setImageResource(android.R.drawable.ic_media_pause);
        }
        holder.tv.setText(currentFile.getName());
        final File f = getItem(position);
        holder.b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("clicked", f.getAbsolutePath());
                FileManagerOpenHelper helper = new FileManagerOpenHelper(context,null,1);
                SQLiteDatabase db = helper.getWritableDatabase();
                ContentValues cv = new ContentValues();
                cv.put(FileManagerOpenHelper.FAV_TABLE_CREATION_TIME,new Date().toString());
                cv.put(FileManagerOpenHelper.FAV_TABLE_FILE_PATH,f.getAbsolutePath());
                db.insert(FileManagerOpenHelper.FAV_TABLE,null,cv);
            }
        });
        return convertView;
    }
}
