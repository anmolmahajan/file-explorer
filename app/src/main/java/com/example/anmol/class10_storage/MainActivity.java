package com.example.anmol.class10_storage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    File currentFolder;
    FileArrayAdapter adapter;
    String path;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = getIntent();
        path = "/";
        if(i == null) {
            path = "/";
        }else{
            Bundle b = i.getExtras();
            if(b != null) {
                path = b.getString("directory");
            }else{
                path = "/";
            }
        }
        if(path == null){
            path = "/";
        }
        currentFolder = new File(path);
        ListView lv = (ListView) findViewById(R.id.fileListView);
        File[] file;
        if(currentFolder.listFiles() == null){
            file = new File[0];
        }else{
            file = currentFolder.listFiles();
        }
        adapter = new FileArrayAdapter(this, file);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileClicked = adapter.getItem(position);
                if(fileClicked.isDirectory()){
                    Intent i = new Intent();
                    i.setClass(MainActivity.this, MainActivity.class);
                    i.putExtra("directory", fileClicked.getAbsolutePath());
                    startActivity(i);

                }else{
                    try{
                        Scanner s = new Scanner(fileClicked);
                        String line = s.nextLine();
                        Log.i("Fetched ", line);
                    }catch (FileNotFoundException e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.action_fav){
            Intent i = new Intent();
            i.setClass(this,FavoritesActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}
